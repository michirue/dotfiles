#!/bin/zsh
export DOTFILES_INSTALL_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
dotfiles=$DOTFILES_INSTALL_DIR

echo "Installing OMZ..."
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh

echo 'Symlinking config files...'
source $dotfiles/symlink-dotfiles.sh

echo "Installing fonts..."
mkdir $dotfiles/tmp
mkdir $dotfiles/tmp/fonts
git clone https://github.com/powerline/fonts.git $dotfiles/tmp/fonts
source $dotfiles/tmp/fonts/install.sh

echo "Installing Vundle..."
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

echo "Installing VIM colors..."
mkdir -p $HOME/.vim/colors
curl -o $HOME/.vim/colors/wombat.vim https://raw.githubusercontent.com/vim-scripts/Wombat/master/colors/wombat.vim

if [[ `uname` == 'Darwin' ]]; then #Only on Mac
	echo "Installing Homebrew..."
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

    echo "Brewing stuff..."
    brew tap caskroom/versions
    brew update
    brew install zsh
    brew install python
    brew install pyenv
    brew install pyenv-virtualenv
    brew install ruby
    brew install rbenv
    brew install jenv
    brew install git
    brew install openssl
    brew install scala

    brew cask install sublime-text
    brew cask install google-chrome
    brew cask install skype
    brew cask install telegram
    brew cask install whatsapp
    brew cask install cyberduck
    brew cask install intellij-idea-ce
    brew cask install datagrip
    brew cask install dropbox
    brew cask install scroll-reverser
    brew cask install java
    brew cask install osxfuse
    brew cask install iterm2
    brew cask install spectacle

    echo "Symlinking Sublime Text 3 Preferences..."
    sublime_settings_dir="$HOME/Library/Application Support/Sublime Text 3/Packages/User"
    link $dotfiles/sublime/Preferences.sublime-settings "$sublime_settings_dir/Preferences.sublime-settings"
    link "$dotfiles/sublime/Package Control.sublime-settings" "$sublime_settings_dir/Package Control.sublime-settings"
    link "$dotfiles/sublime/Terminal.sublime-settings" "$sublime_settings_dir/Terminal.sublime-settings"
    link "$dotfiles/sublime/Open.sublime-settings" "$sublime_settings_dir/Open.sublime-settings"

elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then #Only on Linux
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/linuxbrew/go/install)"
fi

rm -rf $dotfiles/tmp

echo "Configuring global GIT properties..."
git config --global core.excludesfile ~/.gitignore_global
git config --global user.name "Michael Rueppel"

echo "Sourcing .zshrc..."
cd ~
source .zshrc
