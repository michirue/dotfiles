#!/bin/bash

dotfiles=$DOTFILES_INSTALL_DIR
echo $dotfiles

link() {
  from="$1"
  to="$2"
  echo "Linking '$from' to '$to'"
  rm -f "$to"
  ln -s "$from" "$to"
}

for src in $(find "$dotfiles/home" -name '.*'); do
  symlinkName="${src##*/}"
  link "$src" "$HOME/$symlinkName"
done
