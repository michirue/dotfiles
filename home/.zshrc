export ZSH=~/.oh-my-zsh
export ZSH_CUSTOM=~/dotfiles/oh-my-zsh/custom
export HOMEBREW_GITHUB_API_TOKEN="01dbe4f7fb1ca4dd0244db68a98c70ee9a534656"

if [[ -f $HOME/.zshrc-local ]]; then
    source $HOME/.zshrc-local
fi

ZSH_THEME="avit-mod"

plugins=(git)

source $ZSH/oh-my-zsh.sh

export PATH="/usr/local/sbin:$PATH"
export PATH="/usr/local/opt/node@/bin:$PATH"

if hash jenv 2>/dev/null; then
    export PATH="$HOME/.jenv/bin:$PATH"
    eval "$(jenv init -)"
fi

if hash pyenv 2>/dev/null; then
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi
