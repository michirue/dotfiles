" Vundle Configuration start
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'L9'
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'Raimondi/delimitMate'
Plugin 'scrooloose/syntastic'
Plugin 'ervandew/supertab'
Plugin 'dbext.vim'
Plugin 'derekwyatt/vim-scala'
Plugin 'berdandy/ansiesc.vim'

call vundle#end()
filetype plugin indent on
" Vundle Configuration end

" vim-airline start
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled=1
" vim-airline end

" dbext start
" let g:dbext_default_profile_sqlite_test = 'type=SQLITE:dbname=$HOME/dev/test.db'
" let g:dbext_default_profile = 'sqlite_test'
" dbext end

:syntax on

set backspace=2

set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

colorscheme wombat


